import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Users from '../views/Users.vue'
import Plans from '../views/Plans.vue'
import Profile from '../views/Profile.vue'
import Edit from '../views/Edit.vue'
import User from '../components/User.vue'
import Diet from '../components/Diet.vue'
import Training from '../components/Training.vue'
import Form from '../components/Form.vue'
import TrainingUser from '../components/TrainingUser.vue'
import DietUser from '../components/DietUser.vue'
import TrainingAdmin from '../components/TrainingAdmin.vue'
import NotFounded from '../views/NotFounded.vue'

import store from '../store/index.js'

Vue.use(VueRouter)

const routes = [
  {
    path: '*',
    component: NotFounded
  },
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/users',
    name: 'Users',
    component: Users,
    beforeEnter: (to, from, next) => {
      if (store.state.isLoggedIn) {
        next()
      } else {
        next('/')
      }
    }
  },
  {
    path: '/plans',
    name: 'Plans',
    component: Plans,
    beforeEnter: (to, from, next) => {
      if (store.state.isLoggedIn) {
        next()
      } else {
        next('/')
      }
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    beforeEnter: (to, from, next) => {
      if (store.state.isLoggedIn) {
        next()
      } else {
        next('/')
      }
    }
  },
  {
    path: '/edit',
    name: 'Edit',
    component: Edit,
    beforeEnter: (to, from, next) => {
      if (store.state.isLoggedIn) {
        next()
      } else {
        next('/')
      }
    }
  },


  // Components
  {
    path: '/user/:id',
    name: 'User',
    component: User,
    beforeEnter: (to, from, next) => {
      if (store.state.isLoggedIn) {
        next()
      } else {
        next('/')
      }
    }
  },
  {
    path: '/diet/:id',
    name: 'Diet',
    component: Diet,
    beforeEnter: (to, from, next) => {
      if (store.state.isLoggedIn) {
        next()
      } else {
        next('/')
      }
    }
  },
  {
    path: '/training/:id',
    name: 'Training',
    component: Training,
    beforeEnter: (to, from, next) => {
      if (store.state.isLoggedIn) {
        next()
      } else {
        next('/')
      }
    }
  },
  {
    path: '/form',
    name: 'Form',
    component: Form,
    beforeEnter: (to, from, next) => {
      if (store.state.isLoggedIn) {
        next()
      } else {
        next('/')
      }
    }
  },
  {
    path: '/traininguser',
    name: 'TrainingUser',
    component: TrainingUser,
    beforeEnter: (to, from, next) => {
      if (store.state.isLoggedIn) {
        next()
      } else {
        next('/')
      }
    }
  },
  {
    path: '/dietuser/:id',
    name: 'DietUser',
    component: DietUser,
    beforeEnter: (to, from, next) => {
      if (store.state.isLoggedIn) {
        next()
      } else {
        next('/')
      }
    }
  },
  {
    path: '/trainingadmin/:id',
    name: 'TrainingAdmin',
    component: TrainingAdmin,
    beforeEnter: (to, from, next) => {
      if (store.state.isLoggedIn) {
        next()
      } else {
        next('/')
      }
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior() {
    document.getElementById('app').scrollIntoView();
  }
})

export default router
