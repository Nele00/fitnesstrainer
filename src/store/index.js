import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLoggedIn: false,
    isAdmin:false,
  },
  mutations: {
    CHANGE_LOGIN(state, payload) {
      state.isLoggedIn = payload;
    },
    CHECK_ADMIN(state, payload){
      state.isAdmin = payload;
    }
  },
  actions: {
    change_login(store, payload) {
      store.commit('CHANGE_LOGIN', payload);
    },
    check_admin(store, payload){
      store.commit('CHECK_ADMIN', payload);
    }
  },
  modules: {
  }
})
